/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./templates/**/*.{html,js}"],
  theme: {
    extend: {
      backgroundPosition: {
        'top-center': 'top center'
      },
      gridTemplateColumns: {
        '2-auto': 'auto 1fr',
        '4-auto': 'auto 1fr auto 1fr',
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require("daisyui"),
  ],
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: "#87423B",
          secondary: "#2D3D53",
          accent: "#504744",
          neutral: "#4C505A",
          "base-100": "#D1CEC6",
        }
      },
      // {
      //   mydark: {
      //     primary: "#87423B",
      //     secondary: "#2D3D53",
      //     accent: "#504744",
      //     neutral: "#4C505A",
      //     // "base-100": "#5B4B3E",
      //     "base-100": "#352e1d",
      //   }
      // },
    ],
    // themes: ['bumblebee', 'forest'],
    // darkTheme: "mydark",
  }
}
